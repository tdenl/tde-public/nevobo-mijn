module.exports = {
  bracketSameLine: true,
  order: process.env.ORDER ? process.env.ORDER : 'smacss',
  printWidth: 100,
  singleQuote: true,
};
